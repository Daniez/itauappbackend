package com.daniel.itau.ItauApp.service;

public interface ParsingService {

    /**
     *  Servicio para parsear la respuesta del json y convertila a mi clase del model (Pelicula)
     *
     * */
    Object parse(String url);

    Object editObject(String url, Object objeto);
}
