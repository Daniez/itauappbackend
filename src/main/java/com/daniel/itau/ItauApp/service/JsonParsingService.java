package com.daniel.itau.ItauApp.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Service
public class JsonParsingService implements ParsingService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper firebaseObjectMapper;

    @Override
    public Object parse(String url) {
        return restTemplate.getForObject(url, Object.class);
    }

    @Override
    public Object editObject(String url, Object objeto) {
        return restTemplate.postForObject(url, objeto, Object.class);
    }


}
