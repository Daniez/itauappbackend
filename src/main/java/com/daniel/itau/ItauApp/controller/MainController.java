package com.daniel.itau.ItauApp.controller;

import com.daniel.itau.ItauApp.model.Pelicula;
import com.daniel.itau.ItauApp.service.ParsingService;
import com.daniel.itau.ItauApp.service.PeliculaService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,  RequestMethod.PUT,  RequestMethod.DELETE})
@RestController
@RequestMapping("/firebase")
public class MainController {


    @Autowired
    private ParsingService parsingService;

    @Autowired
    private RestTemplate restTemplate;

    private List<Pelicula> peliculas;
    private List<Pelicula> lista;

    private  static final String URL_FIREBASE = "https://itauapp-72e70.firebaseio.com/peliculas.json";
    private  static final String URL_EDIT_FIREBASE = "https://itauapp-72e70.firebaseio.com/peliculas/";



    @GetMapping("/peliculasFirebase")
    public List<Pelicula> listar() {
        lista = new ArrayList<>();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("peliculas");
        llenarLista(ref, lista) ;
        return  lista ;
    }

    private void llenarLista(DatabaseReference ref, List<Pelicula> lista ) {
        ref.addChildEventListener(new PeliculaService() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Pelicula pelicula = dataSnapshot.getValue(Pelicula.class);
                pelicula.setIdFirebase(dataSnapshot.getKey());
                lista.add(pelicula);
            }
        });
    }
    @GetMapping("/peliculas")
    public List<Pelicula> listarRest() throws IOException {
        peliculas = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        HashMap<String, Object> datos = (HashMap) parsingService.parse(URL_FIREBASE+"?OrderBy='id'");
        for (Map.Entry<String, Object> entry : datos.entrySet()) {
            String key = entry.getKey();
            Pelicula value = mapper.readValue(new Gson().toJson(entry.getValue()), Pelicula.class);
            value.setIdFirebase(key);
            peliculas.add(value);

        };
        peliculas.sort((p1,p2)-> p1.getId().compareTo(p2.getId()));
        return peliculas;
    }

    @RequestMapping(value = "/peliculas/crear", method = RequestMethod.POST)
    @ResponseStatus(code = HttpStatus.CREATED)
    public Pelicula createPeliculas(@RequestBody Pelicula pelicula) throws IOException {
        DatabaseReference ref = conexion();
        if(existePelicula(pelicula, false)) return pelicula;
        ref.push().setValueAsync(pelicula);
        return pelicula;
    }

    @RequestMapping(value = "/peliculas/editar", method = RequestMethod.PUT)
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public Pelicula editarPelicula(@RequestBody Pelicula pelicula) throws IOException {
        DatabaseReference ref = conexion();
        if(existePelicula(pelicula, false)){
            String key = pelicula.getIdFirebase();
            pelicula.setIdFirebase(null);
            HttpEntity<Pelicula> entity = headers(pelicula);
            restTemplate.exchange(URL_EDIT_FIREBASE + "/" +  key + ".json",  HttpMethod.PUT, entity, String.class).getBody();
        }
        return pelicula;
    }

    @RequestMapping(value = "/peliculas/pelicula/{id}", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public Pelicula editarPelicula(@PathVariable Long id) throws IOException {
        Pelicula pelicula = new Pelicula();
        pelicula.setId(id);
        DatabaseReference ref = conexion();
        existePelicula(pelicula, true);
        return pelicula;
    }

    @RequestMapping(value = "/peliculas/eliminar/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public Pelicula eliminarPelicula(@PathVariable Long id) throws IOException {
        Pelicula pelicula = new Pelicula();
        pelicula.setId(id);
        DatabaseReference ref = conexion();
        if(existePelicula(pelicula, true)){
            String key = pelicula.getIdFirebase();
            pelicula.setIdFirebase(null);
            HttpEntity<Pelicula> entity = headers(pelicula);
            restTemplate.exchange(URL_EDIT_FIREBASE + "/" +  key + ".json",  HttpMethod.DELETE, entity, String.class).getBody();
        }
        return pelicula;
    }

    private HttpEntity<Pelicula> headers(@RequestBody Pelicula pelicula) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return new HttpEntity<Pelicula>(pelicula, headers);
    }

    private boolean existePelicula(@RequestBody Pelicula pelicula, boolean consulta) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        HashMap<String, Object> datos = (HashMap) parsingService.parse(URL_FIREBASE + "?orderBy=\"id\"&equalTo="+ pelicula.getId());
        for (Map.Entry<String, Object> entry : datos.entrySet()) {
            if (consulta) {
                Pelicula peliculaTemp = mapper.readValue(new Gson().toJson(entry.getValue()), Pelicula.class);
                pelicula.setId(peliculaTemp.getId());
                pelicula.setAutor(peliculaTemp.getAutor());
                pelicula.setTitle(peliculaTemp.getTitle());
                pelicula.setDescripcion(peliculaTemp.getDescripcion());
            }
            pelicula.setIdFirebase(entry.getKey());
        };
        return datos.size() > 0;
    }

    private DatabaseReference conexion() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        return database.getReference("peliculas");
    }

}
