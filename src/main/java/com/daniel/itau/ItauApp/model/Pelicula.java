package com.daniel.itau.ItauApp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties //Para Ignorar las propiedades del json que no hacen match con el objeto
public class Pelicula {

    private Long id;
    private String idFirebase;
    private String title;
    private String descripcion;
    private String autor;

    public Pelicula(Long id, String title, String descripcion, String autor) {
        this.title = title;
        this.descripcion = descripcion;
        this.autor = autor;
        this.id = id;
    }

    public Pelicula() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdFirebase() {
        return idFirebase;
    }

    public void setIdFirebase(String idFirebase) {
        this.idFirebase = idFirebase;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pelicula)) return false;
        Pelicula pelicula = (Pelicula) o;
        return Objects.equals(getId(), pelicula.getId()) &&
                Objects.equals(getIdFirebase(), pelicula.getIdFirebase()) &&
                Objects.equals(getTitle(), pelicula.getTitle()) &&
                Objects.equals(getDescripcion(), pelicula.getDescripcion()) &&
                Objects.equals(getAutor(), pelicula.getAutor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getIdFirebase(), getTitle(), getDescripcion(), getAutor());
    }
}
